import { ErrorHandler, Injectable, NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import * as Sentry from "@sentry/browser";
import { environment } from "src/environments/environment";
import { AppComponent } from "./app.component";

Sentry.init({
  dsn: "https://40b16b16a9ab46b8ace72165e89d77aa@o389562.ingest.sentry.io/5248570",
  release: environment.version
});

@Injectable()
export class SentryErrorHandler implements ErrorHandler {
  constructor() {}
  handleError(error) {
    Sentry.captureException(error.originalError || error);
    throw error;
  }
}

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule],
  providers: [{ provide: ErrorHandler, useClass: SentryErrorHandler }],
  bootstrap: [AppComponent]
})
export class AppModule {}
